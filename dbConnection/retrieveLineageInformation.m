function trackingMatrix = retrieveLineageInformation(connDB, projectId)

curs = exec(connDB, ['SELECT id, location, parent_id, radius, confidence, location_t, location_c, skeleton_id FROM treenode WHERE project_id = ' '''' num2str(projectId) '''' ' ORDER BY location_t']);
curs = fetch(curs);

disp(['total number of database entries: ' num2str(size(curs.Data, 1))]);

% We follow the SWC convention (more or less, since we need time): [id, type, x, y, z, radius, parent_id, time, confidence, skeletonId]
trackingMatrix = zeros(size(curs.Data, 1), 9);

checkpoints = round((size(curs.Data, 1) * 0.1):(size(curs.Data, 1) * 0.1):size(curs.Data, 1));
counter = 1;

for kk = 1:size(curs.Data, 1)
    xyz = char(curs.Data{kk, 2});
    xyz = str2num(xyz(2:end-1));
    
    parentId = curs.Data{kk, 3};
    if isnan(parentId)
        parentId = -1;
    end;
    
    trackingMatrix(kk, :) = [curs.Data{kk, 1} 0 xyz curs.Data{kk, 4} parentId curs.Data{kk, 6} curs.Data{kk, 5}];
    
    if kk == checkpoints(counter)
        if counter < 10
            disp([num2str(counter * 10, '%.2d') '% of database entries parsed']);
            counter = counter + 1;
        else
            disp('all database entries parsed');
        end;
    end;
end;

close(curs);