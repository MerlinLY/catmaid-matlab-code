function [projectId, stackId, stackSize, stackRes] = retrieveProjectInformation(connDB, projectTitle)

setdbprefs('DataReturnFormat','cellarray');

curs = exec(connDB, ['SELECT id FROM project WHERE title = ''' projectTitle '''']);
curs = fetch(curs);

if isempty(curs.Data)
    error(['No project found with title ' projectTitle])
end;

projectId = curs.Data{1};

% Retrieve stack information from project
curs = exec(connDB, ['SELECT stack_id FROM project_stack WHERE project_id = ' '''' num2str(projectId) '''']);
curs = fetch(curs);

stackId = curs.Data{1};

curs = exec(connDB, ['SELECT dimension, resolution FROM stack WHERE id = ' '''' num2str(stackId) '''']);
curs = fetch(curs);

stackSize = char(curs.Data{1});
stackSize = str2num(stackSize(2:end-1));

stackRes = char(curs.Data{2});
stackRes = str2num(stackRes(2:end-1));


%Database connections, cursors, and resultset objects remain open until you close them using the close function. 
%Always close a cursor, connection, or resultset when you finish using it. Close a cursor before closing the connection used for that cursor.
close(curs);