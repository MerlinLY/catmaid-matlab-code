function [TP, FP, FN, N_GT, LA, N_LA] = compareCATMAIDgroundTruthToTGMMresult(projectName, tag, TGMMbasename, avgNucleusRadius)

%default variables to test
if( nargin < 1 )
    projectName = 'SiMView2_2012-01-22_Pdu-H2BeGFP_AutomaticTracking_003'
    tag = 'ground truth'
    TGMMbasename = 'E:\temp\GMEMtracking3D_1411401533_Tau16\XML_finalResult_lht\GMEMfinalResult_frame'  
    avgNucleusRadius = 5;%in um. It is used to decide if ground truth and TGMM output correspond to the same cell
end


%%
%retrieve ground truth from CATMAID database
addpath '../dbConnection/'

%retrieve nodes with specific tag signaling groudn truth
[~, skeletonId] = retrieveDatabaseIDwithSpecificTag(projectName, tag);


%retrieve information to connect to the database
[username, password, databasename, databaseURL] = readDatabaseInfo();
connDB = connectToDatabase(databasename, databaseURL, username, password);
[ projectId, stackId, stackSize, stackRes ] = retrieveProjectInformation( connDB, projectName);

%retrieve each of the skeletons
trackingMatrixGT = [];
for ii = 1:length(skeletonId)
    WHEREclause = [ 'project_id=' num2str(projectId) ' AND skeleton_id=' num2str(skeletonId(ii))];
    trackingMatrixAux = retrieveLineageFilter( connDB, WHEREclause );
    trackingMatrixGT = [trackingMatrixGT; trackingMatrixAux];
end

clear trackingMatrixAux;
minT = min(trackingMatrixGT(:,8));
maxT = max(trackingMatrixGT(:,8));

close(connDB);

rmpath '../dbCOnnection/'
%%
%retreieve output from TGMM code
addpath '../readTGMM_XMLoutput/'

D = dir([TGMMbasename '*.xml']);

frameIni = str2double(D(1).name(end-7:end-4));
frameIni = max(frameIni, minT);
frameEnd = str2double(D(end).name(end-7:end-4));
frameEnd = min(frameEnd, maxT);

trackingMatrixTGMM = parseMixtureGaussiansXml2trackingMatrixCATMAIDformat(TGMMbasename,frameIni,frameEnd);

%parse xyz into nm (the same as CATMAID)
for ii = 3:5
trackingMatrixTGMM(:,ii) = stackRes(ii-2) * trackingMatrixTGMM(:,ii);
end

rmpath '../readTGMM_XMLoutput/'
%%
%compare both outputs

%build hasj table to find parents efficiently
treenodeIdGT = containers.Map(trackingMatrixGT(:,1),1:size(trackingMatrixGT,1));
treenodeIdTGMM = containers.Map(trackingMatrixTGMM(:,1),1:size(trackingMatrixTGMM,1));

%variables to store results
TP = zeros(frameEnd - frameIni + 1,1);%one to one correspondence
TN = TP;%not possible to measure without complete ground truth
FP = TP;%when we have more than one TGMM centroid close to a GT centroid
FN = TP;%when we do not have any TGMM centroid close to a GT centroid
N_GT = TP;%number of GT elements per time point
LA = TP;%number of correct linkage per time point
N_LA = TP;%total number of linkages in GT per time point
mapGT2TGMM = zeros(size(trackingMatrixGT,1),1);

%main loop
for frame = frameIni:frameEnd
   pos = find(trackingMatrixGT(:,8) == frame );
   if( isempty(pos) == true )
       continue;
   end
   xyzGT = trackingMatrixGT(pos,3:5) / 1000;%in um
   parGT = trackingMatrixGT(pos, 7);
   posGT = pos;
   
   pos = find(trackingMatrixTGMM(:,8) == frame );
   if( isempty(pos) == true )
       continue;
   end
   xyzTGMM = trackingMatrixTGMM(pos,3:5) / 1000;%in um
   parTGMM = trackingMatrixTGMM(pos, 7);
   posTGMM = pos;
   
   [idx, dist] = rangesearch(xyzTGMM, xyzGT, avgNucleusRadius);
   
   %update matching map
   for ii = 1:length(idx)
       if( isempty( idx{ii} ) == false )
           mapGT2TGMM( posGT(ii) ) = posTGMM( idx{ii}(1) );
       end
   end   
   ll = cellfun(@length, idx);
   
   TP(frame - frameIni + 1) = sum(ll >= 1);%when we have oversegmentation the nuclei is still detected (so the closest centroid is a TP)
   pos = find( ll > 1 );
   FP(frame - frameIni + 1) = sum(ll(pos)) - length(pos);
   FN(frame - frameIni + 1) = sum(ll == 0);
   N_GT(frame - frameIni + 1) = size(xyzGT,1);
      
   %checl linkage accuracy: we use the closest point
   for ii = 1:length(idx)
       parId = parGT(ii);
       if( isKey(treenodeIdGT, parId) )
           parIdxGT = treenodeIdGT(parId);
       else
           parIdxGT = -1;
       end
       
       if( isempty( idx{ii} ) == false )
           parId = parTGMM( idx{ii}(1) );
           if( isKey(treenodeIdTGMM, parId) )
               parIdxTGMM = treenodeIdTGMM(parId);
           else
               parIdxTGMM = -1;
           end
       else
           parIdxTGMM = -1;
       end
        
       if(parIdxTGMM == -1 && parIdxGT == -1)
           %nothing to do or account
       elseif( parIdxTGMM == -1 && parIdxGT~= -1 )%error in linkahe
           N_LA(frame - frameIni + 1) = N_LA(frame - frameIni + 1) + 1;
       elseif( parIdxTGMM ~= -1 && parIdxGT == -1 )
           %nothing to do( Ihave no way to account for these errors but
           %they should not 
       else
           N_LA(frame - frameIni + 1) = N_LA(frame - frameIni + 1) + 1;
           %check if it is correct
           if( mapGT2TGMM( parIdxGT ) == parIdxTGMM )%correct
               LA(frame - frameIni + 1) = LA(frame - frameIni + 1) + 1;
           end
       end
   end
   
end

%%
%display results
disp(['==============Results======================'])
disp(['Total nuclei detected = ' num2str(sum(TP))])
disp(['Total oversegmentations = ' num2str(sum(FP))])
disp(['Total nuclei not detected = ' num2str(sum(FN))])
disp(['Linkage accuracy between consecutive time points = ' num2str(sum(LA) / sum(N_LA))])

