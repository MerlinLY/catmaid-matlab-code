%trackingM contains id, type, x, y, z, radius, parent_id, time, confidence, skeleton_id
function [trackingM, erase] = deleteShortLivedTracks(trackingM, thr, isErasable)

N = size(trackingM,1);
nodeIdMap = containers.Map(trackingM(:,1),[1:N]);



%map children for each node
chIdx = -ones(N,2);

for kk = 1: N
    par = trackingM(kk,7);
    
    if( par < 0 || isKey(nodeIdMap, par)== false )
        continue;
    end
    aux = nodeIdMap( par );
    
    if( chIdx(aux,1) < 0 )
        chIdx(aux,1) = kk;
    else
        chIdx(aux,2) = kk;
    end
end

%for each root node, check thhe length of the maximum branch
pos = find( trackingM(:,7) < 0 );%last frame should not be counted as death

erase = zeros(round(N/10),1);
eraseN = 0;
numF = 0;
for kk = 1: length(pos)
    
    queue = pos(kk);
    eraseAux = pos(kk);
    iniTM = trackingM(pos(kk),8);
    endTM = iniTM;
    while( isempty(queue) == false && (endTM-iniTM)<thr)
       nodeIdx = queue(1);
       queue(1) = [];
       
       %update elements
       eraseAux = [eraseAux nodeIdx];
       endTM = max([trackingM(nodeIdx,8) endTM]);
       if( chIdx(nodeIdx,1) >0 )
           queue = [queue chIdx(nodeIdx,1)];
       end
       if( chIdx(nodeIdx,2) >0 )
           queue = [queue chIdx(nodeIdx,2)];
       end              
    end    
    
    if( (endTM-iniTM) <thr && endTM ~= max(trackingM(:,8)))%we exit earlier and it is not the end of the tracking
        
        %check if it is erasable
       aux = sum( isErasable(eraseAux) == true ) / length( eraseAux );
       if( aux > 0.9 )
           erase(eraseN+1 : eraseN + length(eraseAux)) = eraseAux;
           eraseN = eraseN + length(eraseAux);
           numF = numF + 1;
       end
    end
end

erase(eraseN+1:end) = [];

%delete filtered elements
trackingM(erase,:) = [];

disp(['Erased ' num2str(numF) ' short length tracks out of ' num2str(length(pos)) ' root nodes']);

