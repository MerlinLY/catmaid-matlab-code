treenodeIdMap = containers.Map(trackingMatrix(:,1),[1:size(trackingMatrix,1)]);

delta_xyz = zeros(size(trackingMatrix,1),1);
n = 0;
for kk = 1:size(trackingMatrix,1)
   
    zz = zeros(5,3);
    idx = kk;
    par = trackingMatrix(idx,7);    
    for aa = 1:5
        
        if( isKey(treenodeIdMap, par) == true )
            
            xyz = trackingMatrix(idx,3:5) ./ stackRes;
            xyzPar = trackingMatrix(treenodeIdMap(par),3:5) ./ stackRes;
            
            zz(aa,:) = (xyz - xyzPar);
            
            idx = treenodeIdMap(par);
            par = trackingMatrix(idx,7);
        else
            break
        end
        
    end
    
    if( aa == 5)
        n = n + 1;
        delta_xyz(n,:) = sum(std(zz));
    end
end
delta_xyz(n+1:end) = [];