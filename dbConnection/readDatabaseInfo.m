function [username, password, databasename, databaseURL] = readDatabaseInfo()

pathstr = fileparts( mfilename('fullpath') );
filename = [pathstr '/databaseInfo.txt'];

fid = fopen(filename,'r');
username = fgetl(fid);
password = fgetl(fid);
databasename = fgetl(fid);
databaseURL = fgetl(fid);

fclose(fid);