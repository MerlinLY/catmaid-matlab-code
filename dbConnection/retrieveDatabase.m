%retrieve information to connect to the database
[username, password, databasename, databaseURL] = readDatabaseInfo();

projectName  = '12-08-28 Drosophila TGMM TM300';

disp(' ');
disp(['connecting to database ' databaseURL]);
connDB = connectToDatabase(databasename, databaseURL, username, password);

disp(' ');
disp(['retrieving information for project ' projectName]);
[projectId, stackId, stackSize, stackRes] = retrieveProjectInformation(connDB, projectName);

disp(' ');
disp('retrieving tracking matrix');
% We follow the SWC convention (more or less, since we need time): [id, type, x, y, z, radius, parent_id, time, confidence]
trackingMatrix = retrieveLineageInformation(connDB, projectId);

disp(' ');
disp('saving tracking matrix');
save([projectName '.mat']);
disp(' ');


close(connDB);